import React, { useEffect } from "react";
import {
  openModal,
  closeModal,
  selectOrders,
  updateOrderStatus,
  selectIsLoading,
} from "./Features/modalSlice";
import { fetchOrders } from "./Thunks/orderListThunk";
import Modal from "./components/Modal/Modal";
import { useDispatch, useSelector } from "react-redux";
import Header from "./components/Header";
import OrderIdDetails from "./components/OrderIdDetails";
import OrderDetails from "./components/OrderDeatails";
import OrderSearchbar from "./components/OrderSearchbar";
import AddOrderItem from "./components/AddOrderItem";
import OrderList from "./components/OrderList";

function App() {
  const dispatch = useDispatch();
  const orders = useSelector(selectOrders);
  const isLoading = useSelector(selectIsLoading);

  useEffect(() => {
    dispatch(fetchOrders());
  }, [dispatch]);

  const handleOpenModal = (id, productName) => {
    dispatch(openModal({ id, productName }));
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  const handleDoneClick = (orderId) => {
    dispatch(updateOrderStatus({ orderId, status: "Approved" }));
  };

  const handleYesClick = (orderId) => {
    dispatch(updateOrderStatus({ orderId, status: "Missing" }));
  };
  return (
    <>
      <Header />
      <OrderIdDetails />

      <div className="w-full h-full bg-gray-100">
        <div className="mx-32 flex flex-col gap-8 py-8 items-center h-full">
          <OrderDetails />

          <div className="bg-white w-full custom-box-shadow rounded-md p-8 flex flex-col">
            <div className="flex w-full items-center justify-between">
              <OrderSearchbar />
              <AddOrderItem />
            </div>
            <OrderList
              isLoading={isLoading}
              orders={orders}
              handleDoneClick={handleDoneClick}
              handleOpenModal={handleOpenModal}
            />
          </div>
        </div>

        <Modal
          isOpen={openModal}
          onClose={handleCloseModal}
          onYesClick={() => handleYesClick()}
        />
      </div>
    </>
  );
}

export default App;
