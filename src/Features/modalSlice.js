import { createSlice } from "@reduxjs/toolkit";
import { fetchOrders } from "../Thunks/orderListThunk";

export const modalSlice = createSlice({
  name: "modal",
  initialState: {
    isModalOpen: false,
    modalAction: null,
    orders: [],
    selectedId: null,
    selectedName: "",
    isLoading: false,
  },
  reducers: {
    openModal: (state, action) => {
      state.isModalOpen = true;
      state.selectedId = action.payload.id;
      state.selectedName = action.payload.productName;
    },
    closeModal: (state) => {
      state.isModalOpen = false;
    },
    setModalAction: (state, action) => {
      state.modalAction = action.payload;
    },
    updateOrderStatus: (state, action) => {
      const { orderId, status } = action.payload;
      let tempId = orderId;
      if (!orderId) {
        tempId = state.selectedId;
      }
      const order = state.orders.find((order) => order.id === tempId);
      if (order) {
        order.status = status;
      }
      state.isModalOpen = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchOrders.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchOrders.fulfilled, (state, action) => {
        state.isLoading = false;
        state.orders = action.payload;
      });
  },
});

export const { openModal, closeModal, setModalAction, updateOrderStatus } =
  modalSlice.actions;
export const selectIsModalOpen = (state) => state.modal.isModalOpen;
export const selectModalAction = (state) => state.modal.modalAction;
export const selectOrders = (state) => state.modal.orders;
export const selectIsLoading = (state) => state.modal.isLoading;

export default modalSlice.reducer;
