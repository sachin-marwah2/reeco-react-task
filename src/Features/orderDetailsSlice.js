import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const orderDetailsSlice = createSlice({
  name: "orderDetails",
  initialState: {
    data: null,
    loading: false,
    error: null,
  },
  reducers: {
    fetchOrderDetailsStart(state) {
      state.loading = true;
    },
    fetchOrderDetailsSuccess(state, action) {
      state.loading = false;
      state.data = action.payload;
    },
    fetchOrderDetailsFailure(state, action) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  fetchOrderDetailsStart,
  fetchOrderDetailsSuccess,
  fetchOrderDetailsFailure,
} = orderDetailsSlice.actions;

export default orderDetailsSlice.reducer;
