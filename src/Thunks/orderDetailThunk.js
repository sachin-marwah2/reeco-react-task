import { createAsyncThunk } from "@reduxjs/toolkit";
import {
  fetchOrderDetailsFailure,
  fetchOrderDetailsSuccess,
} from "../Features/orderDetailsSlice";

export const fetchOrderDetails = createAsyncThunk(
  "orderDetails/fetchOrderDetails",
  async (_, { dispatch }) => {
    try {
      const response = await import("../../orderDetail.json");
      dispatch(fetchOrderDetailsSuccess(response.default));
    } catch (error) {
      dispatch(fetchOrderDetailsFailure(error.message));
    }
  }
);
