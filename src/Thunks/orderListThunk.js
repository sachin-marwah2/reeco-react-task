import { createAsyncThunk } from "@reduxjs/toolkit";
import ordersData from "../orders.json";

export const fetchOrders = createAsyncThunk("modal/fetchOrders", async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(ordersData);
    }, 1000);
  });
});
