import React from "react";

const AddOrderItem = () => {
  return (
    <div className="flex w-max items-center gap-12">
      <button className="px-6 py-2 cursor-pointer border-green-900 border-2 rounded-full">
        Add Item
      </button>
      <div>Print</div>
    </div>
  );
};

export default AddOrderItem;
