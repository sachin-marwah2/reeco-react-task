import React from "react";

const Header = () => {
  return (
    <header className="flex text-white w-full h-full px-32 justify-between items-center p-2 bg-green-900">
      <nav>
        <ul className="flex space-x-14 items-center">
          <h1 className="text-2xl font-bold mr-4">Reeco</h1>
          <li className="opacity-2">
            <a href="#" className="text-white">
              Store
            </a>
          </li>
          <li className="opacity-2">
            <a href="#" className="text-white">
              Orders
            </a>
          </li>
          <li className="opacity-2">
            <a href="#" className="text-white">
              Analytics
            </a>
          </li>
        </ul>
      </nav>
      <div className="flex gap-32">
        <h3>Cart</h3>
        <h3>Hello, James</h3>
      </div>
    </header>
  );
};

export default Header;
