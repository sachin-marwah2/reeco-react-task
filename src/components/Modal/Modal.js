import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModal, selectIsModalOpen } from "../../Features/modalSlice";

const Modal = ({ onYesClick }) => {
  const dispatch = useDispatch();
  const isOpen = useSelector(selectIsModalOpen);

  const selectedName = useSelector((state) => state.modal.selectedName);

  const handleYes = () => {
    onYesClick();
    handleCloseModal();
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };
  if (!isOpen) return null;
  return (
    <>
      <div className="fixed top-0 left-0 w-full h-full bg-gray-800 opacity-50 z-50"></div>
      <div className="fixed w-96 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 bg-white p-8 rounded shadow-lg z-50">
        <div className="flex items-center justify-between">
          <h2 className="text-xl font-bold">Missing Product</h2>
          <button onClick={handleCloseModal}>X</button>
        </div>
        <h3 className="my-4 max-w-8 truncate">{selectedName}</h3>
        <div className="flex justify-end gap-10">
          <button onClick={handleYes}>Yes</button>
          <button onClick={handleCloseModal}>No</button>
        </div>
      </div>
    </>
  );
};

export default Modal;
