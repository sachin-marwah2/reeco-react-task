import React, { useEffect } from "react";
import orderDetails from "../../orderDetail.json";
import { useDispatch, useSelector } from "react-redux";
import { fetchOrderDetails } from "../Thunks/orderDetailThunk";

const OrderDetails = () => {
  const dispatch = useDispatch();
  const { data, loading, error } = useSelector((state) => state.orderDetails);

  useEffect(() => {
    dispatch(fetchOrderDetails());
  }, [dispatch]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <div className="bg-white w-full custom-box-shadow rounded-md p-8 flex">
      {Object.entries(orderDetails).map(([key, value], index, array) => (
        <div
          className={`flex-1 px-9 ${
            index === array.length - 1 ? "" : "border-r"
          } flex flex-col gap-2`}
          key={key}
        >
          <h3 className="text-gray-600 text-base font-bold">
            {capitalizeFirstLetter(key)}
          </h3>
          <h1 className="text-black text-2xl font-bold">{value}</h1>
        </div>
      ))}
    </div>
  );
};

export default OrderDetails;
