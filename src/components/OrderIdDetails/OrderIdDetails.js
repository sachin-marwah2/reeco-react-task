import React from "react";

const OrderDetails = () => {
  return (
    <div className="w-full h-full shadow-md bg-white p-6 flex flex-col gap-2 mx-auto px-32">
      <h3 className="leading-10 text-xl">
        Orders <span className="font-bold underline">Order 3247ABC</span>
      </h3>
      <div className="flex items-center justify-between">
        <h1 className="font-bold text-2xl">Order 324571BC</h1>
        <div className="flex gap-4">
          <button className="px-6 py-2 cursor-pointer border-green-900 border-2 rounded-full">
            Back
          </button>
          <button className="px-6 py-2 cursor-pointer text-white bg-green-900 border-none rounded-full">
            Approve Order
          </button>
        </div>
      </div>
    </div>
  );
};

export default OrderDetails;
