import React from "react";
import Avocado from "../../assets/Images/Avocado.jpg";

const OrderList = ({ orders, handleDoneClick, handleOpenModal, isLoading }) => {
  return (
    <>
      {isLoading ? (
        <div>Loading...</div>
      ) : (
        <div class="overflow-x-auto">
          <div class="w-full">
            <div class="bg-white shadow-md overflow-hidden rounded my-6">
              <table class="min-w-max w-full table-auto">
                <thead>
                  <tr class="bg-gray-200 text-gray-600 uppercase text-sm leading-normal">
                    <th class="py-3 px-6 text-center">Product name</th>
                    <th class="py-3 px-6 text-left">Brand</th>
                    <th class="py-3 px-6 text-left">Price</th>
                    <th class="py-3 px-6 text-left">Quantity</th>
                    <th class="py-3 px-6 text-left">Total</th>
                    <th class="py-3 px-6 text-left">Status</th>
                    <th class="py-3 px-6 text-left"></th>
                  </tr>
                </thead>
                <tbody class="text-gray-600 text-sm font-light">
                  {orders.map((order) => (
                    <tr key={order.id} class="border-b border-gray-200">
                      <td class="py-3 w-max px-6 text-left whitespace-nowrap">
                        <div class="flex w-max m-0 items-center">
                          <div class="mr-2">
                            <img className="w-20 h-24" src={Avocado}></img>
                          </div>
                          <span class="font-medium text-base">
                            {order.productName}
                          </span>
                        </div>
                      </td>
                      <td class="py-3 px-6 text-left">
                        <span class="font-medium text-base">{order.brand}</span>
                      </td>
                      <td class="py-3 px-6 text-center">
                        <span class="font-medium text-base">{order.price}</span>
                      </td>
                      <td class="py-3 px-6 text-center">
                        <span class="font-medium text-base">
                          {order.quantity}
                        </span>
                      </td>
                      <td class="py-3 px-6 text-center">
                        <span class="font-medium text-base">{order.total}</span>
                      </td>
                      <td class="py-3 px-6 text-center">
                        {order.status === "Approved" && (
                          <h3 class="px-6 py-2 cursor-pointer text-white bg-green-500 border-none rounded-full">
                            {order.status}
                          </h3>
                        )}
                        {order.status === "Missing" && (
                          <h3 class="px-6 py-2 cursor-pointer text-white bg-red-500 border-none rounded-full">
                            {order.status}
                          </h3>
                        )}
                      </td>
                      <td class="py-3 px-6 text-center">
                        <div class="flex font-medium text-base item-center gap-4">
                          <button
                            onClick={() => handleDoneClick(order.id)}
                            className="border-none outline-none bg-transparent cursor-pointer text-green-700"
                          >
                            Done
                          </button>
                          <button
                            onClick={() =>
                              handleOpenModal(order.id, order.productName)
                            }
                            className="border-none outline-none font-bold bg-transparent cursor-pointer text-red-700"
                          >
                            X
                          </button>
                          <button className="border-none outline-none bg-transparent cursor-pointer text-gray-700">
                            Edit
                          </button>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default OrderList;
