import React from "react";

const OrderSearchbar = () => {
  return (
    <div className="w-2/5">
      <input
        placeholder="Search..."
        type="search"
        className="w-full px-4 py-2 rounded-full outline-none border-gray-200 border"
      />
    </div>
  );
};

export default OrderSearchbar;
