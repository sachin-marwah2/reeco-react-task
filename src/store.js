// store.js
import { configureStore } from "@reduxjs/toolkit";
import modalReducer from "./Features/modalSlice";
import orderDetailsReducer from "./Features/orderDetailsSlice";

export const store = configureStore({
  reducer: {
    modal: modalReducer,
    orderDetails: orderDetailsReducer,
  },
});
