/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      opacity: {
        1: "0.30",
        2: "0.50",
      },
    },
  },
  plugins: [],
};
